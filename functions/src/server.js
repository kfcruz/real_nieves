const express = require('express')
const exphbs = require('express-handlebars')
const path = require('path')
const morgan = require('morgan')
const favicon = require('serve-favicon')

// Initializations
const app = express()

// Setings
app.set('port', process.env.PORT || 5000);
app.set('views', path.join(__dirname, 'views'))
app.engine('.hbs', exphbs({
    defaultLayout: 'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs'
}))
app.set('view engine', '.hbs')

// Middlewares
app.use(favicon(path.join(__dirname, '../../public', 'favicon.ico')))
app.use(morgan('dev'))
app.use(express.urlencoded({ extended: false }))
    // Global Variables

// Routes
app.use(require('./routes/index.routes'))

// Static files
app.use(express.static(path.join(__dirname, '../../public')))

module.exports = app