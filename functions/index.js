require('dotenv').config();
const app = require('./src/server');
const functions = require('firebase-functions')


app.listen(app.get('port'), () => {
    console.log(`Server on port:` + app.get('port'))
})

exports.app = functions.https.onRequest(app)